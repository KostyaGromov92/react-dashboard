export function addInArrayAtPosition(array, element, position) {
  const arrayCopy = [...array];
  arrayCopy.splice(position, 0, element);
  return arrayCopy;
}

export function replaceElementOfArray(array) {
  return function (options) {
    return array.map((element) => (options.when(element) ? options.for(element) : element));
  };
}

export function addColumn(board, column) {
  return {
    ...board,
    columns: addInArrayAtPosition(board.columns, column, board.columns.length),
  };
}

export function addCard(board, inColumnId, card, { on } = {}) {
  const columnToAdd = board.columns.find(({ id }) => id === inColumnId);

  const cards = addInArrayAtPosition(
    columnToAdd.cards,
    card,
    on === 'top' ? 0 : columnToAdd.cards.length,
  );
  const columns = replaceElementOfArray(board.columns)({
    when: ({ id }) => inColumnId === id,
    for: (value) => ({ ...value, cards }),
  });
  return { ...board, columns };
}

export function removeColumn(board, columnId) {
  return {
    ...board,
    columns: board.columns.filter(({ id }) => id !== columnId),
  };
}

export function formatDate({ isFullDate, currentDateString = '', isDateTime = false }) {
  const dateData = isFullDate ? new Date() : new Date(currentDateString);
  const currentMonth = dateData.getMonth() + 1;
  const currentMonthFormatted = currentMonth < 9 ? `0${currentMonth}` : currentMonth;
  const currentDate = dateData.getDate() < 9 ? `0${dateData.getDate()}` : dateData.getDate();
  const currentHours = dateData.getHours() < 9 ? `0${dateData.getHours()}` : dateData.getHours();
  const currentMinutes = dateData.getMinutes() < 10 ? `0${dateData.getMinutes()}` : dateData.getMinutes();

  const formattedDate = isDateTime
    ? `${dateData.getFullYear()}-${currentMonthFormatted}-${currentDate}T11:00`
    : `${dateData.getFullYear()}-${currentMonthFormatted}-${currentDate}`;


  return {
    formatDate: formattedDate,
    month: currentMonthFormatted,
    date: currentDate,
    hours: currentHours,
    minutes: currentMinutes,
  };
}
