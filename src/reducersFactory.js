// @flow

import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

// REDUCERS
import dashBoard from './reducers/dashBoard';
import infoDate from './reducers/infoDate';
import notification from './reducers/notification';

export default combineReducers({
  routing,
  dashBoard,
  infoDate,
  notification,
});
