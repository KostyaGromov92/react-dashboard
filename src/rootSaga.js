// @flow

import { fork } from 'redux-saga/effects';

import * as infoDateSagas from './sagas/infoDate';

export default function* rootSaga(): Generator<*, *, *> {
  yield fork(infoDateSagas.watchGetInfoDateSaga);
}
