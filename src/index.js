// @flow

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import CssBaseline from '@material-ui/core/CssBaseline';

// STYLES
import './index.css';

// COMPONENTS
import App from './App';
import Routes from './Routes';

// STORE
import Store from './store';

const root = document.getElementById('root');

if (root) {
  ReactDOM.render(
    <React.StrictMode>
      <Provider store={Store}>
        <CssBaseline />
        <Router>
          <App>
            <Routes />
          </App>
        </Router>
      </Provider>
    </React.StrictMode>,
    root,
  );
}
