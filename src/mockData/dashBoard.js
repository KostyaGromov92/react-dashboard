/* eslint-disable import/prefer-default-export */
export const DASH_BOARD_DATA = [
  {
    id: 0,
    title: 'Default dashboard',
    columns: [
      {
        id: 0,
        title: 'Backlog',
        cards: [
          {
            id: 1,
            title: 'Card title 1',
            description: 'Card content',
            status: 'Backlog',
          },
          {
            id: 2,
            title: 'Card title 2',
            description: 'Card content',
          },
          {
            id: 3,
            title: 'Card title 3',
            description: 'Card content',
          },
        ],
      },
      {
        id: 1,
        title: 'Doing',
        cards: [
          {
            id: 9,
            title: 'Card title 9',
            description: 'Card content',
          },
        ],
      },
      {
        id: 2,
        title: 'Q&A',
        cards: [
          {
            id: 10,
            title: 'Card title 10',
            description: 'Card content',
          },
          {
            id: 11,
            title: 'Card title 11',
            description: 'Card content',
          },
        ],
      },
      {
        id: 3,
        title: 'Production',
        cards: [
          {
            id: 12,
            title: 'Card title 12',
            description: 'Card content',
          },
          {
            id: 13,
            title: 'Card title 13',
            description: 'Card content',
          },
        ],
      },
    ],
  },
  {
    id: 1,
    title: 'Second dashboard',
    columns: [
      {
        id: 0,
        title: 'Backlog',
        cards: [
          {
            id: 1,
            title: 'Card title 1',
            description: 'Card content',
            status: 'Backlog',
          },
          {
            id: 2,
            title: 'Card title 2',
            description: 'Card content',
          },
        ],
      },
      {
        id: 1,
        title: 'Doing',
        cards: [
          {
            id: 9,
            title: 'Card title 9',
            description: 'Card content',
          },
        ],
      },
      {
        id: 2,
        title: 'Q&A',
        cards: [
          {
            id: 10,
            title: 'Card title 10',
            description: 'Card content',
          },
          {
            id: 11,
            title: 'Card title 11',
            description: 'Card content',
          },
        ],
      },
    ],
  },
];
