/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// INFO DATE DATA - SELECTORS
// =============================================================================

import type { InfoDateState } from '../reducers/infoDate';

type State = { infoDate: InfoDateState };

export const getIsFetchingInfoDate = (state: State) => (state.infoDate ? state.infoDate.isFetching : false);
export const getInfoDateData = (state: State) => (state.infoDate ? state.infoDate.infoDateData : null);
export const getInfoDateError = (state: State) => (state.infoDate ? state.infoDate.error : null);
