/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// DASH BOARD DATA - SELECTORS
// =============================================================================


import type { DashBoardDataState } from '../reducers/dashBoard';

type State = { dashBoard: DashBoardDataState };

export const getDashBoardData = (state: State) => (state.dashBoard ? state.dashBoard.dashBoardData : []);
export const getCurrentDashBoardData = (state: State) => (state.dashBoard ? state.dashBoard.currentDashboard : []);

export const getCurrentCardData = (state: State) => (state.dashBoard ? state.dashBoard.currentCard : {});
