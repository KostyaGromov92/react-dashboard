/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow
// NOTIFICATION - SELECTORS
// =============================================================================

import type { NotificationState } from '../reducers/notification';

type State = { notification: NotificationState };

export const getNotificationData = (state: State) => (state.notification ? state.notification.notificationData : false);
export const getIsOpenNotification = (state: State) => (state.notification ? state.notification.isOpen : null);
