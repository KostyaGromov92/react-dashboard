// @flow

import React from 'react';
import Container from '@material-ui/core/Container';

import 'react-app-polyfill/ie9'; // For IE 9-11 support
import 'react-app-polyfill/ie11'; // For IE 11 support

type Props = {
  children: Array<*> | Object,
}

const App = (props: Props) => {
  const { children } = props;

  return (
    <main>
      <Container>
        {children}
      </Container>
    </main>
  );
};

export default App;
