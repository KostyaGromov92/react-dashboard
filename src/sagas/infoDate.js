/* eslint-disable max-len */
// @flow
// ABOUT US DATA - SAGAS
// =============================================================================

import type { PutEffect, IOEffect } from 'redux-saga/effects';
import {
  call, put, takeEvery, delay,
} from 'redux-saga/effects';

// TYPES
import * as infoDateActionTypes from '../actions/types/infoDate';
import type { FluxStandardAction } from '../types/flux';

// ACTIONS
import * as infoDateAction from '../actions/infoDate';
import * as notificationAction from '../actions/notification';

// SERVICES
import { processRequest } from '../services/Api';

export function* handleGetInfoDateSaga(action: FluxStandardAction): Generator<PutEffect, *, *> {
  try {
    const { payload } = action;

    if (payload) {
      const { infoDate } = payload;

      const requestData = {
        url: `${infoDate.date}/${infoDate.month}/date`,
      };

      const response = yield call(processRequest, requestData);

      yield put(infoDateAction.getInfoDateSuccess(response.data));
      yield put(notificationAction.handleShowNotification({
        type: 'success',
        title: 'Данные загрузились',
        text: response,
      }));
      yield delay(3000);
      yield put(notificationAction.handleHideNotification());
    }
  } catch (e) {
    yield put(infoDateAction.getInfoDateFailure(e));
    yield put(notificationAction.handleShowNotification({
      type: 'error',
      title: 'Ошибка',
      text: 'Извините, произошла ошибка при загрузке данных',
    }));
    yield delay(3000);
    yield put(notificationAction.handleHideNotification());
  }
}


export function* watchGetInfoDateSaga(): Generator<IOEffect, *, *> {
  yield takeEvery(infoDateActionTypes.GET_INFO_DATE_REQUEST, handleGetInfoDateSaga);
}
