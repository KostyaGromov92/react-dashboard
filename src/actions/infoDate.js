/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow

// INFO DATE - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as infoDateTypes from './types/infoDate';

export const getInfoDateRequest = createAction(infoDateTypes.GET_INFO_DATE_REQUEST);
export const getInfoDateSuccess = createAction(infoDateTypes.GET_INFO_DATE_SUCCESS);
export const getInfoDateFailure = createAction(infoDateTypes.GET_INFO_DATE_FAILURE);

export const handleShowNotification = createAction(infoDateTypes.SHOW_NOTIFICATION);
export const handleHideNotification = createAction(infoDateTypes.HIDE_NOTIFICATION);

export const clearInfoDateError = createAction(infoDateTypes.CLEAR_INFO_DATE_ERROR);
