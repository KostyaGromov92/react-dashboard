/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow

// NOTIFICATION - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as notificationTypes from './types/notification';


export const handleShowNotification = createAction(notificationTypes.SHOW_NOTIFICATION);
export const handleHideNotification = createAction(notificationTypes.HIDE_NOTIFICATION);
