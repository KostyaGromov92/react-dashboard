// @flow

// NOTIFICATION - ACTION TYPES
// =============================================================================

export const HIDE_NOTIFICATION = 'HIDE_NOTIFICATION';
export const SHOW_NOTIFICATION = 'SHOW_NOTIFICATION';
