// @flow

// INFO DATE - ACTION TYPES
// =============================================================================

export const GET_INFO_DATE_REQUEST = 'GET_INFO_DATE_REQUEST';
export const GET_INFO_DATE_SUCCESS = 'GET_INFO_DATE_SUCCESS';
export const GET_INFO_DATE_FAILURE = 'GET_INFO_DATE_FAILURE';

export const HIDE_NOTIFICATION = 'HIDE_NOTIFICATION';
export const SHOW_NOTIFICATION = 'SHOW_NOTIFICATION';

export const CLEAR_INFO_DATE_ERROR = 'CLEAR_INFO_DATE_ERROR';
