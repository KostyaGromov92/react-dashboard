/* eslint-disable max-len */
/* eslint-disable import/prefer-default-export */
// @flow

// DASHBOARD DATA - ACTIONS
// =============================================================================

import { createAction } from 'redux-actions';

import * as dashBoardTypes from './types/dashBoard';

export const handleAddNewDashBoardCard = createAction(dashBoardTypes.DASH_BOARD_HANDLE_ADD_NEW_CARD);
export const handleMoveDashBoardCard = createAction(dashBoardTypes.DASH_BOARD_HANDLE_MOVE_CARD);

export const handleRemoveDashBoardColumn = createAction(dashBoardTypes.DASH_BOARD_HANDLE_REMOVE_COLUMN);
export const handleMoveDashBoardColumn = createAction(dashBoardTypes.DASH_BOARD_HANDLE_MOVE_COLUMN);
export const handleAddNewDashBoardColumn = createAction(dashBoardTypes.DASH_BOARD_HANDLE_ADD_NEW_COLUMN);

export const setCurrentDashBoard = createAction(dashBoardTypes.SET_CURRENT_DASH_BOARD);
export const handleCreateNewDashBoard = createAction(dashBoardTypes.HANDLE_CREATE_NEW_DASH_BOARD);

export const handleFinishDashBoardCard = createAction(dashBoardTypes.HANDLE_FINISH_DASH_BOARD_CARD);

export const setCurrentCardData = createAction(dashBoardTypes.SET_CURRENT_CARD_DATA);
export const handleSaveCardComment = createAction(dashBoardTypes.HANDLE_SAVE_CARD_COMMENT);
