/* eslint-disable max-len */
/* eslint-disable react/prefer-stateless-function */
// @flow

import React, { Component, Suspense, lazy } from 'react';
import { withRouter } from 'react-router-dom';

// COMPONENTS
import DashBoardRoute from './components/Routes/DashboardRoute';
import MainRoute from './components/Routes/MainRoute';

// CONTAINERS
const HomeContainer = lazy(() => import('./containers/HomeContainer'));
const DashBoardContainer = lazy(() => import('./containers/DashBoardContainer'));


class Routes extends Component<*, *> {
  render() {
    return (
      <Suspense fallback={null}>
        <div>
          <MainRoute path="/home" exact component={HomeContainer} />

          <DashBoardRoute path="/dashboard" component={DashBoardContainer} />
        </div>
      </Suspense>
    );
  }
}

export default withRouter(Routes);
