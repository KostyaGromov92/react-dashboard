
import { makeStyles } from '@material-ui/core/styles';

// eslint-disable-next-line import/prefer-default-export
export const useStyles = makeStyles(() => ({
  dashBoardSection: {
    marginTop: '30px',
  },

  button: {
    margin: '0 10px',
  },

  card: {
    backgroundColor: '#fff',
    display: 'flex',
    flexDirection: 'column',
    minWidth: '300px',
    maxWidth: '300px',
    borderRadius: '4px',
    padding: '15px',
    boxShadow: '1px 1px 15px 1px rgba(0, 0, 0, 0.1)',
    marginBottom: '10px',
  },

  cardItemTitle: {
    fontSize: '12px',
    marginRight: '5px',
    color: 'gray',
  },

  titleBoard: {
    textAlign: 'center',
  },

  topBlock: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: '40px',
  },

  columnTitleBlock: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: '10px',
  },

  lastItem: {
    padding: '10px 0',
    borderTop: '1px solid lightgray',
    marginTop: '10px',
  },

  isDoneBlock: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: '10px',
  },
}));
