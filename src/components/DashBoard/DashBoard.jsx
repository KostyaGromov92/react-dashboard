// @flow

import React from 'react';
import Board from '@lourenci/react-kanban';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import classnames from 'classnames';

// STYLES
import { useStyles } from './styles';

// CONSTANTS
import {
  ADD_NEW_CARD_MODAL, ADD_NEW_DASH_BOARD_MODAL, ADD_NEW_COLUMN_MODAL, CURRENT_CARD_MODAL,
} from '../../constants/dashBoard';

type Props = {
  dashBoardData: Array<Object>,
  currentDashBoardData: Object,
  handleOpenModal: (modalType: string) => void,
  handleSelectDashBoard: () => void,
  handleCardMove: (card: Object, source: Object, distance: Object) => void,
  handleColumnMove: (card: Object, source: Object, distance: Object) => void,
  handleRemoveColumn: (columnId: string | number) => void,
  handleOpenCardModal: (
    columnId: string | number, cardId: string | number, modalType: string
  ) =>void,
};

const DashBoard = ({
  dashBoardData,
  currentDashBoardData,
  handleCardMove,
  handleColumnMove,
  handleRemoveColumn,
  handleOpenModal,
  handleOpenCardModal,
  handleSelectDashBoard,
}: Props) => {
  const classes = useStyles();

  const handClickCard = (columnId, cardId) => {
    handleOpenCardModal(columnId, cardId, CURRENT_CARD_MODAL);
  };

  return (
    <section className={classes.dashBoardSection}>
      <div className={classes.topBlock}>
        <Button
          onClick={() => handleOpenModal(ADD_NEW_COLUMN_MODAL)}
          variant="contained"
          color="primary"
          className={classes.button}
        >
          Добавить колонку
        </Button>
        <Button
          onClick={() => handleOpenModal(ADD_NEW_CARD_MODAL)}
          variant="contained"
          color="primary"
          className={classes.button}
        >
          Добавить задачу
        </Button>
        <Button
          onClick={() => handleOpenModal(ADD_NEW_DASH_BOARD_MODAL)}
          variant="contained"
          color="primary"
          className={classes.button}
        >
          Добавить новую доску
        </Button>
        <div>
          <select onChange={handleSelectDashBoard}>
            {dashBoardData.map((dashboard) => (
              <option
                selected={currentDashBoardData.title === dashboard.title}
                key={dashboard.id}
                value={dashboard.id}
              >
                {dashboard.title}

              </option>
            ))}
          </select>
        </div>
      </div>
      <h6
        className={classes.titleBoard}
      >
        Вы можете двигать колонки. Неведеите и удерживайте мышкой название колонки

      </h6>
      <Board
        renderCard={({
          id, description, status, columnId, topic, finishDate, isDone, comment, finishDateTime,
        }) => (
          <div
            role="button"
            tabIndex={0}
            onClick={() => handClickCard(columnId, id)}
            onKeyPress={() => handClickCard(columnId, id)}
            className={classnames(classes.card, isDone && classes.doneCard)}
          >
            <h6>{topic}</h6>
            <div className={classes.cardItem}>
              <span className={classes.cardItemTitle}>Описание:</span>
              <span>{description}</span>
            </div>
            <div className={classes.cardItem}>
              <span className={classes.cardItemTitle}>Дата завершения:</span>
              <span>{finishDate}</span>
            </div>
            <div className={classes.cardItem}>
              <span className={classes.cardItemTitle}>Статус:</span>
              <span>{status}</span>
            </div>

            <div className={classnames(classes.cardItem, classes.lastItem)}>
              <span>{comment}</span>
              <div className={classes.isDoneBlock}>
                <span className={classes.cardItemTitle}>{isDone && 'Задача завершена:'}</span>
                <span>{finishDateTime}</span>
              </div>
            </div>
          </div>
        )}
        onColumnDragEnd={handleColumnMove}
        allowRemoveColumn
        onCardDragEnd={handleCardMove}
        renderColumnHeader={({ title, id }) => (
          <div className={classes.columnTitleBlock}>
            <h6>{title}</h6>

            <IconButton
              onClick={() => handleRemoveColumn(id)}
              className={classes.addButton}
              color="primary"
              aria-label="add column"
            >
              <HighlightOffIcon />
            </IconButton>
          </div>
        )}
      >
        {currentDashBoardData}
      </Board>
    </section>
  );
};

export default DashBoard;
