// @flow

import React from 'react';
import { Alert, AlertTitle } from '@material-ui/lab';

// STYLES
import { useStyles } from './styles';

type Props = {
  type: string,
  title: string,
  text: string,
}

const Popup = ({ type, title, text }: Props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Alert severity={type}>
        <AlertTitle>{title}</AlertTitle>
        {text}
      </Alert>
    </div>
  );
};

export default Popup;
