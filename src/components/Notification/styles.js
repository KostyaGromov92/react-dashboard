/* eslint-disable import/prefer-default-export */
import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: '300px',
    position: 'fixed',
    bottom: '25px',
    right: '25px',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}));
