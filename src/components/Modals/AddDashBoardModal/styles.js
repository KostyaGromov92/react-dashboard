
import { makeStyles } from '@material-ui/core/styles';

// eslint-disable-next-line import/prefer-default-export
export const useStyles = makeStyles((theme) => ({
  textFiled: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(2),
  },

  buttonGroup: {
    display: 'flex',
    flexDirection: 'row',
  },

  button: {
    marginRight: theme.spacing(2),
  },

  columnTitleBlock: {
    display: 'flex',
    alignItems: 'center',
  },

  addButton: {
    marginLeft: theme.spacing(2),
  },

  columnsList: {
    listStyleType: 'none',
    margin: 0,
    display: 'flex',
    flexDirection: 'row',
    padding: 0,
    flexWrap: 'wrap',
  },

  columnListItem: {
    border: '1px solid #3f51b5',
    borderRadius: '30px',
    padding: '2px 10px',
    position: 'relative',
    marginRight: '15px',
    marginBottom: '10px',
  },

  columnListButton: {
    padding: '5px',
    marginLeft: '5px',
  },
}));
