// @flow

import React, { useState } from 'react';
import CancelIcon from '@material-ui/icons/Cancel';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';

// STYLES
import { useStyles } from './styles';

type Props = {
  dashBoardList: Array<Object>,
  handleAddDashBoard: (dashBoardData: Object) => void,
  handleCloseModal: () => void
}

const AddDashBoardModal = ({ dashBoardList, handleCloseModal, handleAddDashBoard }: Props) => {
  const classes = useStyles();

  const [dashBoardTitle, setDashBoardTitle] = useState('');
  const [columnTitle, setColumnTitle] = useState('');
  const [columns, setColumns] = useState([]);

  const handleChangeDashBoardTitle = (event: any) => {
    setDashBoardTitle(event.target.value);
  };

  const handleChangeColumnTitle = (event: any) => {
    setColumnTitle(event.target.value);
  };

  const handleAddColumn = () => {
    const columnId = columns.length === 0 ? 0 : columns[columns.length - 1].id + 1;
    if (columnTitle === '') return false;

    const columnsData = {
      id: columnId,
      title: columnTitle,
      cards: [],
    };

    setColumns([...columns, columnsData]);
    setColumnTitle('');

    return true;
  };

  const handleRemoveColumn = (columnId: string | number) => {
    const columnsData = columns.filter((column) => column.id !== columnId);

    setColumns([...columnsData]);
  };

  const handleCreateNewDashBoard = () => {
    const dashBoardId = dashBoardList.length === 0
      ? 0 : dashBoardList[dashBoardList.length - 1].id + 1;

    const dashBoardData = {
      id: dashBoardId,
      title: dashBoardTitle,
      columns,
    };

    handleAddDashBoard(dashBoardData);
    handleCloseModal();
  };


  return (
    <>
      <h3>Создать новую доску</h3>
      <TextField
        value={dashBoardTitle}
        id="outlined-basic"
        fullWidth
        className={classes.textFiled}
        label="Название доски"
        variant="outlined"
        onChange={handleChangeDashBoardTitle}
      />
      <div className={classes.columnTitleBlock}>
        <TextField
          value={columnTitle}
          id="outlined-basic"
          className={classes.textFiled}
          fullWidth
          label="Название колонки"
          variant="outlined"
          onChange={handleChangeColumnTitle}
        />
        <IconButton
          onClick={() => handleAddColumn()}
          className={classes.addButton}
          color="primary"
          aria-label="add column"
        >
          <AddIcon />
        </IconButton>
      </div>
      {columns.length > 0 && (
        <ul className={classes.columnsList}>
          {columns.map((columnItem) => (
            <li className={classes.columnListItem} key={columnItem.id}>
              {columnItem.title}
              <IconButton
                onClick={() => handleRemoveColumn(columnItem.id)}
                className={classes.columnListButton}
                color="primary"
                aria-label="remove column"
              >
                <CancelIcon fontSize="small" color="primary" />
              </IconButton>
            </li>
          ))}
        </ul>
      )}
      <div className={classes.buttonGroup}>
        <Button
          onClick={handleCreateNewDashBoard}
          variant="contained"
          color="primary"
          disabled={dashBoardTitle.length === 0 || columns.length === 0}
          className={classes.button}
        >
          Создать
        </Button>
        <Button
          onClick={handleCloseModal}
          variant="contained"
          color="primary"
          className={classes.button}
        >
          Отменить
        </Button>
      </div>

    </>
  );
};

export default AddDashBoardModal;
