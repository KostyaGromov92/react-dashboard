// @flow

import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

// STYLES
import { useStyles } from './styles';

type Props = {
  handleCloseModal: () => void,
  handleAddNewColum: (columnTitle: string) => void
}

const AddNewColumnModal = ({
  handleCloseModal, handleAddNewColum,
}: Props) => {
  const classes = useStyles();

  const [columnTitle, setColumnTitle] = useState('');

  const handleChangeColumnTitle = (event: any) => {
    setColumnTitle(event.target.value);
  };

  const handleCreateNewColumn = () => {
    handleAddNewColum(columnTitle);
  };

  return (
    <>
      <h3>Добавление новой колонки</h3>
      <TextField
        value={columnTitle}
        id="outlined-basic"
        fullWidth
        label="Название колонки"
        variant="outlined"
        onChange={handleChangeColumnTitle}
        className={classes.textFiled}
      />
      <div className={classes.buttonGroup}>
        <Button
          className={classes.button}
          onClick={handleCreateNewColumn}
          variant="contained"
          color="primary"
          disabled={columnTitle === ''}
        >
          Добавить
        </Button>
        <Button
          className={classes.button}
          onClick={handleCloseModal}
          variant="contained"
          color="primary"
        >
          Отменить
        </Button>
      </div>
    </>
  );
};

export default AddNewColumnModal;
