// @flow

import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import classNames from 'classnames';

// STYLES
import { useStyles } from './styles';

type Props = {
  isOpenCommentBlock: boolean,
  additionalComment: string,
  currentCardDescription: string,
  handleOpenPopup: () => void,
  handleOpenCommentBlock: () => void,
  handleChangeComment: () => void,
  handleSaveComment: (comment: string) => void,
}

const CardModal = ({
  handleOpenPopup,
  handleChangeComment,
  currentCardDescription,
  isOpenCommentBlock,
  additionalComment,
  handleOpenCommentBlock,
  handleSaveComment,
}: Props) => {
  const classes = useStyles();

  return (
    <>
      <h3>Описание задачи</h3>
      <Typography variant="body1" gutterBottom>
        {currentCardDescription}
      </Typography>
      {isOpenCommentBlock && (
        <div className={classes.commentBlock}>
          <TextField
            value={additionalComment}
            fullWidth
            className={classes.textFiled}
            label="Комментарий"
            variant="outlined"
            onChange={handleChangeComment}
          />
          <Button
            onClick={() => handleSaveComment(additionalComment)}
            variant="contained"
            color="primary"
            disabled={additionalComment.length === 0}
            className={classNames(classes.button, classes.commentButton)}
          >
            Добавить
          </Button>
        </div>
      )}
      <div className={classes.buttonGroup}>
        <Button
          className={classes.button}
          onClick={handleOpenCommentBlock}
          variant="contained"
          disabled={isOpenCommentBlock}
          color="primary"
        >
          Оставить комментарий
        </Button>
        <Button
          className={classes.button}
          onClick={handleOpenPopup}
          variant="contained"
          color="primary"
        >
          Отменить завершенной
        </Button>
      </div>
    </>
  );
};

export default CardModal;
