// @flow

import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

// STYLES
import { useStyles } from './styles';

// COMPONENTS
import Notification from '../../Notification';

// HELPERS
import { formatDate } from '../../../services/helpers';

type Props = {
  isOpenNotification: boolean,
  isFetchingInfoFade: boolean,
  notificationInfoData: Object,
  handleCloseModal: () => void,
  handleGetInfoDate: (infoDate: Object) => void
}

const RegisterModal = ({
  handleCloseModal, handleGetInfoDate, isFetchingInfoFade, isOpenNotification, notificationInfoData,
}: Props) => {
  const classes = useStyles();

  const [userData, setUserData] = useState('');
  const [userPosition, setUserPosition] = useState('');
  const [selectedDate, setSelectedDate] = useState(formatDate({ isFullDate: true }).formatDate);

  const handleDateChange = (event) => {
    setSelectedDate(event.target.value);
  };

  const handleChangeUserData = (event: any) => {
    setUserData(event.target.value);
  };

  const handleChangeUserPosition = (event: any) => {
    setUserPosition(event.target.value);
  };

  const handleOnBlurDate = () => {
    const dateRequest = formatDate({ isFullDate: false, currentDateString: selectedDate });

    if (selectedDate !== '') {
      handleGetInfoDate({ date: dateRequest.date, month: dateRequest.month });
    }
  };

  return (
    <>
      <h3>Регитсрация</h3>
      <TextField
        value={userData}
        fullWidth
        className={classes.textFiled}
        label="Ф.И.О"
        variant="outlined"
        onChange={handleChangeUserData}
      />
      <TextField
        id="date"
        label="Дата рождения"
        onChange={handleDateChange}
        onBlur={handleOnBlurDate}
        helperText={isFetchingInfoFade ? 'Пожадуйста подождите, обробатывается запрос' : ''}
        disabled={isFetchingInfoFade}
        type="date"
        defaultValue={selectedDate}
        className={classes.textField}
        InputLabelProps={{
          shrink: true,
        }}
      />
      <TextField
        value={userPosition}
        className={classes.textFiled}
        fullWidth
        label="Должность"
        variant="outlined"
        onChange={handleChangeUserPosition}
      />
      <div className={classes.buttonGroup}>
        <Button
          className={classes.button}
          onClick={handleCloseModal}
          variant="contained"
          color="primary"
          disabled={
            userData.length === 0
            || selectedDate.length === 0
            || userPosition.length === 0
            || isFetchingInfoFade
          }
        >
          Сохранить
        </Button>
        <Button className={classes.button} onClick={handleCloseModal} variant="contained" color="primary">
          Отмена
        </Button>
      </div>
      {isOpenNotification && (
        <Notification
          type={notificationInfoData.type}
          title={notificationInfoData.title}
          text={notificationInfoData.text}
        />
      )}
    </>
  );
};

export default RegisterModal;
