
import { makeStyles } from '@material-ui/core/styles';

// eslint-disable-next-line import/prefer-default-export
export const useStyles = makeStyles((theme) => ({
  textFiled: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(2),
  },

  select: {
    maxWidth: '350px',
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(2),
  },

  buttonGroup: {
    display: 'flex',
    flexDirection: 'row',
  },

  button: {
    marginRight: '10px',
  },
}));
