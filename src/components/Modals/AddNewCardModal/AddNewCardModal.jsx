// @flow

import React, { useState } from 'react';
import { v1 as uuidv1 } from 'uuid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

// STYLES
import { useStyles } from './styles';

// HELPERS
import { formatDate } from '../../../services/helpers';

type Props = {
  currentDashBoardData: Object,
  handleCloseModal: () => void,
  handleAddNewCard: (cardData: Object) => void
}

const AddNewColumnModal = ({
  currentDashBoardData, handleCloseModal, handleAddNewCard,
}: Props) => {
  const classes = useStyles();

  const [cardTopic, setCardTopic] = useState('');
  const [cardDescription, setCardDescription] = useState('');
  const [selectedDate, setSelectedDate] = useState(formatDate({ isFullDate: true }).formatDate);
  const [selectedColumn, setSelectedColumn] = useState('');

  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  const handleChangeCardTopic = (event: any) => {
    setCardTopic(event.target.value);
  };

  const handleChangeCardDescription = (event: any) => {
    setCardDescription(event.target.value);
  };

  const handleChangeColumnName = (event: any) => {
    setSelectedColumn(event.target.value);
  };

  const handleCreateNewCard = () => {
    const cardData = {
      id: uuidv1(),
      topic: cardTopic,
      description: cardDescription,
      finishDate: selectedDate,
      columnId: selectedColumn === '' ? 0 : selectedColumn,
    };

    handleAddNewCard(cardData);
  };

  return (
    <>
      <h3>Добавление новой задачи</h3>
      <TextField
        value={cardTopic}
        fullWidth
        label="Тема"
        variant="outlined"
        onChange={handleChangeCardTopic}
        className={classes.textFiled}
      />
      <TextField
        value={cardDescription}
        fullWidth
        label="Описание"
        variant="outlined"
        onChange={handleChangeCardDescription}
        className={classes.textFiled}
      />
      <TextField
        id="date"
        label="Дата завершения"
        onChange={handleDateChange}
        type="date"
        defaultValue={selectedDate}
        className={classes.textField}
        InputLabelProps={{
          shrink: true,
        }}
      />

      <select className={classes.select} onChange={handleChangeColumnName}>
        {currentDashBoardData.columns.map((column) => (
          <option value={column.id} key={column.id}>{column.title}</option>
        ))}
      </select>


      <div className={classes.buttonGroup}>
        <Button
          className={classes.button}
          onClick={handleCreateNewCard}
          variant="contained"
          color="primary"
          disabled={cardTopic === '' || cardDescription === ''}
        >
          Добавить
        </Button>
        <Button className={classes.button} onClick={handleCloseModal} variant="contained" color="primary">
          Отменить
        </Button>
      </div>

    </>
  );
};

export default AddNewColumnModal;
