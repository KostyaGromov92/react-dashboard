/* eslint-disable react/jsx-props-no-spreading */
// @flow

import React from 'react';
import { useSelector } from 'react-redux';
import type { ComponentType } from 'react';
import { Redirect, Route } from 'react-router-dom';


// SELECTORS
import { getDashBoardData } from '../../selectors/dashBoard';

type Props = {
  component: ComponentType<any>;
};

function DashBoardRoute({ component: Component, ...props }: Props) {
  const dashBoardData = useSelector((state) => getDashBoardData(state));

  return (
    <Route
      render={() => (dashBoardData.length <= 0 ? <Component {...props} />
        : <Redirect to="/dashboard" />
      )}
    />
  );
}

export default DashBoardRoute;
