// @flow

import React from 'react';
import Button from '@material-ui/core/Button';

// STYLES
import { useStyles } from './styles';

// COMPONENTS
import Modal from '../Modal';
import AddDashBoardModal from '../Modals/AddDashBoardModal';
import RegisterModal from '../Modals/RegisterModal';

const DASH_BOARD_MODAL = 'DASH_BOARD_MODAL';
const REGISTER_MODAL = 'REGISTER_MODAL';

type Props = {
  dashBoardData: Array<Object>,
  isOpenNotification: boolean,
  isModalOpen: boolean,
  isFetchingInfoFade: boolean,
  currentModal: string,
  notificationInfoData: Object,
  handleOpenModal: (modalType: string) => void,
  handleCloseModal: () => void,
  handleAddDashBoard: (dashBoardData: Object) => void,
  handleGetInfoDate: (infoDate: Object) => void
}

const Home = ({
  dashBoardData,
  isModalOpen,
  isOpenNotification,
  notificationInfoData,
  isFetchingInfoFade,
  handleOpenModal,
  handleCloseModal,
  handleAddDashBoard,
  currentModal,
  handleGetInfoDate,
}: Props) => {
  const classes = useStyles();

  const renderModalContent = () => {
    switch (currentModal) {
      case DASH_BOARD_MODAL:
        return (
          <AddDashBoardModal
            handleAddDashBoard={handleAddDashBoard}
            dashBoardList={dashBoardData}
            handleCloseModal={handleCloseModal}
          />
        );

      case REGISTER_MODAL:
        return (
          <RegisterModal
            handleCloseModal={handleCloseModal}
            isFetchingInfoFade={isFetchingInfoFade}
            handleGetInfoDate={handleGetInfoDate}
            isOpenNotification={isOpenNotification}
            notificationInfoData={notificationInfoData}
          />
        );

      default:
        return null;
    }
  };

  return (
    <>
      <div className={classes.root}>
        <Button onClick={() => handleOpenModal(DASH_BOARD_MODAL)} className={classes.button} variant="contained" color="primary">
          Создать доску
        </Button>
        <Button onClick={() => handleOpenModal(REGISTER_MODAL)} className={classes.button} variant="contained" color="primary">
          Регистрация
        </Button>
      </div>
      {isModalOpen && (
        <Modal isModalOpen={isModalOpen} handleCloseModal={handleCloseModal}>
          {renderModalContent()}
        </Modal>
      )}
    </>
  );
};

export default Home;
