// @flow

import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';

// HELPERS
import { formatDate } from '../../../services/helpers';

import Notification from '../../Notification';

// STYLES
import { useStyles } from './styles';

type Props = {
  isOpenPopup: boolean,
  isOpenNotification: boolean,
  isFetchingInfoDate: boolean,
  notificationInfoData: Object,
  handleClosePopup: () => void,
  handleFinishCard: (finishDateTime: string) => void,
  handleGetInfoDate: (infoDate: Object) => void
}

const FinishTaskPopup = ({
  handleClosePopup,
  isOpenPopup,
  isFetchingInfoDate,
  handleGetInfoDate,
  handleFinishCard,
  isOpenNotification,
  notificationInfoData,
}: Props) => {
  const [selectedDate, setSelectedDate] = useState(
    formatDate({ isFullDate: true, isDateTime: true }).formatDate,
  );

  const classes = useStyles();

  const handleDateChange = (event) => {
    setSelectedDate(event.target.value);
  };

  const handleOnBlurDate = () => {
    const dateRequest = formatDate({ isFullDate: false, currentDateString: selectedDate });

    if (selectedDate !== '') {
      handleGetInfoDate({ date: dateRequest.date, month: dateRequest.month });
    }
  };

  const handleFinishCardData = () => {
    handleFinishCard(selectedDate);
  };

  // eslint-disable-next-line no-nested-ternary
  const helperText = isFetchingInfoDate
    ? 'Пожадуйста подождите, обробатывается запрос'
    : new Date(selectedDate) < new Date()
      ? 'Дата завершения не может быть меньше текущей даты, пожалуйста выбирите другую дату'
      : '';

  return (
    <>
      <Dialog
        onClose={handleClosePopup}
        aria-labelledby="simple-dialog-title"
        open={isOpenPopup}
      >
        <DialogTitle id="simple-dialog-title">
          Выбирите дату завершения задачи
        </DialogTitle>
        <div className={classes.contentBlock}>
          <TextField
            label="Дата завершения"
            onChange={handleDateChange}
            onBlur={handleOnBlurDate}
            helperText={helperText}
            disabled={isFetchingInfoDate}
            type="datetime-local"
            defaultValue={selectedDate}
            className={classes.textField}
            InputLabelProps={{
              shrink: true,
            }}
          />
          <Button
            className={classes.button}
            onClick={handleFinishCardData}
            disabled={
              isFetchingInfoDate
              || selectedDate.length === 0
              || new Date(selectedDate) < new Date()
            }
            variant="contained"
            color="primary"
          >
            Завершить
          </Button>
        </div>
        {isOpenNotification && (
          <Notification
            type={notificationInfoData.type}
            title={notificationInfoData.title}
            text={notificationInfoData.text}
          />
        )}
      </Dialog>
    </>
  );
};

export default FinishTaskPopup;
