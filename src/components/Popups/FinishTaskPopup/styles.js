
import { makeStyles } from '@material-ui/core/styles';

// eslint-disable-next-line import/prefer-default-export
export const useStyles = makeStyles((theme) => ({
  root: {
    maxWidth: '300px',
  },

  textFiled: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(2),
  },

  buttonGroup: {
    display: 'flex',
    flexDirection: 'row',
  },

  button: {
    marginTop: '15px',
  },

  contentBlock: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    maxWidth: '300px',
    margin: '0 auto 30px',
  },
}));
