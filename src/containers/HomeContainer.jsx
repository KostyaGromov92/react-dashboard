// @flow
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// COMPONENTS
import Home from '../components/Home';

// ACTIONS
import { handleCreateNewDashBoard } from '../actions/dashBoard';
import { getInfoDateRequest } from '../actions/infoDate';

// SELECTORS
import { getIsFetchingInfoDate } from '../selectors/infoDate';
import { getIsOpenNotification, getNotificationData } from '../selectors/notification';
import { getDashBoardData } from '../selectors/dashBoard';

const HomeContainer = () => {
  const dispatch = useDispatch();

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [currentModal, setCurrentModal] = useState('');

  const isFetchingInfoFade = useSelector((state) => getIsFetchingInfoDate(state));
  const notificationInfoData = useSelector((state) => getNotificationData(state));
  const isOpenNotification = useSelector((state) => getIsOpenNotification(state));
  const dashBoardDataState = useSelector((state) => getDashBoardData(state));

  const handleOpenModal = (modalType: string) => {
    setIsModalOpen(true);
    setCurrentModal(modalType);
  };

  const handleCloseModal = () => {
    setIsModalOpen(false);
  };

  const handleAddDashBoard = (dashBoardData: Object) => {
    dispatch(handleCreateNewDashBoard({ dashBoardData }));
  };

  const handleGetInfoDate = (infoDate: Object) => {
    dispatch(getInfoDateRequest({ infoDate }));
  };

  return (
    <>
      <Home
        isModalOpen={isModalOpen}
        dashBoardData={dashBoardDataState}
        notificationInfoData={notificationInfoData}
        isOpenNotification={isOpenNotification}
        isFetchingInfoFade={isFetchingInfoFade}
        handleGetInfoDate={handleGetInfoDate}
        handleCloseModal={handleCloseModal}
        handleAddDashBoard={handleAddDashBoard}
        handleOpenModal={handleOpenModal}
        currentModal={currentModal}
      />
    </>
  );
};
export default HomeContainer;
