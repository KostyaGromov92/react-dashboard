// @flow
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// COMPONENTS
import DashBoardComponent from '../components/DashBoard';
import Modal from '../components/Modal';
import AddDashBoardModal from '../components/Modals/AddDashBoardModal';
import AddNewColumnModal from '../components/Modals/AddNewColumnModal';
import AddNewCardModal from '../components/Modals/AddNewCardModal';
import FinishTaskPopup from '../components/Popups/FinishTaskPopup';
import CardModal from '../components/Modals/CardModal';
import Notification from '../components/Notification';

// SELECTORS
import { getDashBoardData, getCurrentDashBoardData, getCurrentCardData } from '../selectors/dashBoard';
import { getIsOpenNotification, getNotificationData } from '../selectors/notification';
import { getIsFetchingInfoDate } from '../selectors/infoDate';

// CONSTANTS
import {
  ADD_NEW_CARD_MODAL,
  ADD_NEW_DASH_BOARD_MODAL,
  ADD_NEW_COLUMN_MODAL,
  CURRENT_CARD_MODAL,
} from '../constants/dashBoard';

// ACTIONS
import {
  handleAddNewDashBoardCard,
  handleMoveDashBoardCard,
  handleRemoveDashBoardColumn,
  handleMoveDashBoardColumn,
  handleAddNewDashBoardColumn,
  handleCreateNewDashBoard,
  setCurrentDashBoard,
  handleFinishDashBoardCard,
  setCurrentCardData,
  handleSaveCardComment,
} from '../actions/dashBoard';
import { getInfoDateRequest } from '../actions/infoDate';
import { handleShowNotification, handleHideNotification } from '../actions/notification';

const DashBoardContainer = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isOpenPopup, setIsOpenPopup] = useState(false);
  const [currentModal, setCurrentModal] = useState('');
  const [isOpenCommentBlock, setIsOpenCommentBlock] = useState(false);
  const [additionalComment, setAdditionalComment] = useState('');

  const dashBoardData = useSelector((state) => getDashBoardData(state));
  const currentDashBoardData = useSelector((state) => getCurrentDashBoardData(state));
  const currentCardData = useSelector((state) => getCurrentCardData(state));
  const isFetchingInfoDate = useSelector((state) => getIsFetchingInfoDate(state));

  const notificationInfoData = useSelector((state) => getNotificationData(state));
  const isOpenNotification = useSelector((state) => getIsOpenNotification(state));

  const dispatch = useDispatch();

  const handleCloseModal = () => {
    setIsModalOpen(false);
    setIsOpenCommentBlock(false);
  };

  const handleOpenModal = (modalType: string) => {
    setIsModalOpen(true);
    setCurrentModal(modalType);
  };

  const handleClosePopup = () => {
    setIsOpenPopup(false);
  };

  const handleOpenPopup = () => {
    setIsOpenPopup(true);
    setIsModalOpen(false);
  };

  const handleAddNewCard = (cardData: Object) => {
    dispatch(handleAddNewDashBoardCard({ cardData }));
    handleCloseModal();
  };

  const handleCardMove = (_card, source, destination) => {
    dispatch(handleMoveDashBoardCard({ card: _card, source, destination }));
  };

  const handleFinishCard = (finishDateTime: string) => {
    dispatch(handleFinishDashBoardCard({ finishDateTime }));
    handleClosePopup();
  };

  const handleOpenCardModal = (columnId, cardId, modalType) => {
    dispatch(setCurrentCardData({ columnId, cardId }));
    setIsModalOpen(true);
    setCurrentModal(modalType);
  };

  const handleRemoveColumn = (columnId: string | number) => {
    const currentColumn = currentDashBoardData.columns.find((column) => column.id === columnId);

    if (currentColumn.cards.length > 0) {
      dispatch(handleShowNotification({
        type: 'warning',
        title: 'Извините',
        text: 'Вы не можите удалить колоку пока в ней есть задачи',
      }));

      setTimeout(() => {
        dispatch(handleHideNotification());
      }, 3000);
    } else {
      dispatch(handleRemoveDashBoardColumn({ columnId }));
    }
  };

  const handleColumnMove = (_card: Object, source: Object, destination: Object) => {
    dispatch(handleMoveDashBoardColumn({ source, destination }));
  };

  const handleAddNewColum = (title: string) => {
    const columnId = currentDashBoardData.columns && currentDashBoardData.columns.length === 0
      ? 0
      : currentDashBoardData.columns[currentDashBoardData.columns.length - 1].id + 1;

    const column = {
      id: columnId,
      title,
      cards: [],
    };

    dispatch(handleAddNewDashBoardColumn({ column }));
    handleCloseModal();
  };


  const handleSelectDashBoard = (event: any) => {
    const dashBoardId = event.target.value;

    dispatch(setCurrentDashBoard({ dashBoardId }));
  };

  const handleAddDashBoard = (dashBoard: Object) => {
    dispatch(handleCreateNewDashBoard({ dashBoardData: dashBoard }));
  };

  const handleOpenCommentBlock = () => {
    setIsOpenCommentBlock(true);
  };

  const handleChangeComment = (event: any) => {
    setAdditionalComment(event.target.value);
  };

  const handleSaveComment = (comment: string) => {
    dispatch(handleSaveCardComment({ comment }));
  };

  const handleGetInfoDate = (infoDate: Object) => {
    dispatch(getInfoDateRequest({ infoDate }));
  };

  const renderModalContent = () => {
    switch (currentModal) {
      case ADD_NEW_COLUMN_MODAL:
        return (
          <AddNewColumnModal
            handleAddNewColum={handleAddNewColum}
            handleCloseModal={handleCloseModal}
          />
        );

      case ADD_NEW_DASH_BOARD_MODAL:
        return (
          <AddDashBoardModal
            dashBoardList={dashBoardData}
            handleAddDashBoard={handleAddDashBoard}
            handleCloseModal={handleCloseModal}
          />
        );

      case ADD_NEW_CARD_MODAL:
        return (
          <AddNewCardModal
            handleAddNewCard={handleAddNewCard}
            handleCloseModal={handleCloseModal}
            currentDashBoardData={currentDashBoardData}
          />
        );

      case CURRENT_CARD_MODAL:
        return (
          <CardModal
            handleOpenCommentBlock={handleOpenCommentBlock}
            isOpenCommentBlock={isOpenCommentBlock}
            handleChangeComment={handleChangeComment}
            additionalComment={additionalComment}
            currentCardDescription={currentCardData.description}
            handleSaveComment={handleSaveComment}
            handleOpenPopup={handleOpenPopup}
          />
        );

      default:
        return null;
    }
  };

  return (
    <>
      <DashBoardComponent
        handleOpenModal={handleOpenModal}
        dashBoardData={dashBoardData}
        handleCardMove={handleCardMove}
        handleColumnMove={handleColumnMove}
        handleOpenCardModal={handleOpenCardModal}
        currentDashBoardData={currentDashBoardData}
        handleSelectDashBoard={handleSelectDashBoard}
        handleRemoveColumn={handleRemoveColumn}
        handleAddNewCard={handleAddNewCard}
      />
      {isModalOpen && (
        <Modal isModalOpen={isModalOpen} handleCloseModal={handleCloseModal}>
          {renderModalContent()}
        </Modal>
      )}
      {isOpenNotification && !isOpenPopup && (
        <Notification
          type={notificationInfoData.type}
          title={notificationInfoData.title}
          text={notificationInfoData.text}
        />
      )}
      {isOpenPopup && (
        <FinishTaskPopup
          isOpenPopup={isOpenPopup}
          isOpenNotification={isOpenNotification}
          notificationInfoData={notificationInfoData}
          isFetchingInfoDate={isFetchingInfoDate}
          handleGetInfoDate={handleGetInfoDate}
          handleClosePopup={handleClosePopup}
          handleFinishCard={handleFinishCard}
        />
      )}
    </>
  );
};
export default DashBoardContainer;
