// @flow

/* eslint-disable max-len */
// NOTIFICATION - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as notificationActionTypes from '../actions/types/notification';

export type NotificationState = {
  notificationData: Object,
  isOpen: boolean,
};

export const initialState: NotificationState = {
  notificationData: {},
  isOpen: false,
};

const handlerMap = {

  [notificationActionTypes.SHOW_NOTIFICATION]: (state, action) => {
    const { type, title, text } = action.payload;

    return {
      ...state,
      isOpen: true,
      notificationData: {
        ...state.notificationData,
        type,
        title,
        text,
      },
    };
  },

  [notificationActionTypes.HIDE_NOTIFICATION]: (state) => ({
    ...state,
    isOpen: false,
    notificationData: {},
  }),

};

export default handleActions(handlerMap, initialState);
