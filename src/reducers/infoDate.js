// @flow

/* eslint-disable max-len */
// INFO DATE DATA - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';

// ACTION TYPES
import * as infoDateActionTypes from '../actions/types/infoDate';

export type InfoDateState = {
  infoDateData: ?any,
  isFetching: boolean,
  error: ?Object,
};

export const initialState: InfoDateState = {
  infoDateData: null,
  isFetching: false,
  error: null,
};

const handlerMap = {
  [infoDateActionTypes.GET_INFO_DATE_REQUEST]: (state) => ({
    ...state,
    isFetching: true,
  }),

  [infoDateActionTypes.GET_INFO_DATE_SUCCESS]: (state, action) => ({
    ...state,
    infoDateData: action.payload,
    isFetching: false,
    error: null,
  }),


  [infoDateActionTypes.GET_INFO_DATE_FAILURE]: (state, action) => ({
    ...state,
    error: action.payload,
    isFetching: false,
  }),

  [infoDateActionTypes.CLEAR_INFO_DATE_ERROR]: (state) => ({
    ...state,
    error: null,
  }),
};

export default handleActions(handlerMap, initialState);
