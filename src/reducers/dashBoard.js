// @flow

/* eslint-disable max-len */
// ABOUT DATA - REDUCER
// =============================================================================

import { handleActions } from 'redux-actions';
import { moveCard, moveColumn, addColumn } from '@lourenci/react-kanban';

// ACTION TYPES
import * as dashBoardActionTypes from '../actions/types/dashBoard';

// HELPERS
import { addCard, removeColumn, formatDate } from '../services/helpers';

export type DashBoardDataState = {
  dashBoardData: Array<Object>,
  currentDashboard: Array<Object>,
  currentCard: Object
};

export const initialState: DashBoardDataState = {
  dashBoardData: [],
  currentDashboard: [],
  currentCard: Object,
};

const handlerMap = {
  [dashBoardActionTypes.HANDLE_CREATE_NEW_DASH_BOARD]: (state, action) => {
    const { dashBoardData } = action.payload;

    return {
      ...state,
      currentDashboard: dashBoardData,
      dashBoardData: [...state.dashBoardData, dashBoardData],
    };
  },

  [dashBoardActionTypes.SET_CURRENT_DASH_BOARD]: (state, action) => {
    const { dashBoardId } = action.payload;

    return {
      ...state,
      currentDashboard: state.dashBoardData[dashBoardId],
    };
  },


  [dashBoardActionTypes.DASH_BOARD_HANDLE_ADD_NEW_CARD]: (state, action) => {
    const { cardData } = action.payload;
    const {
      id, topic, finishDate, columnId, description,
    } = cardData;

    const cardStatus = state.currentDashboard.columns.find((column) => column.id === +columnId);

    const updatedBoard = addCard(state.currentDashboard, +columnId, {
      id,
      topic,
      finishDate,
      description,
      status: cardStatus.title,
      columnId,
    });

    return {
      ...state,
      currentDashboard: updatedBoard,
    };
  },

  [dashBoardActionTypes.DASH_BOARD_HANDLE_MOVE_CARD]: (state, action) => {
    const { card, source, destination } = action.payload;

    const updatedBoard = moveCard(state.currentDashboard, source, destination);
    // eslint-disable-next-line max-len
    const currentColumn = updatedBoard.columns.find((column) => column.id === destination.toColumnId);
    const currentCardInColumn = currentColumn.cards.find((cardItem) => cardItem.id === card.id);
    currentCardInColumn.status = currentColumn.title;

    return {
      ...state,
      currentDashboard: updatedBoard,
    };
  },

  [dashBoardActionTypes.DASH_BOARD_HANDLE_REMOVE_COLUMN]: (state, action) => {
    const { columnId } = action.payload;

    const updatedBoard = removeColumn(state.currentDashboard, +columnId);

    return {
      ...state,
      currentDashboard: updatedBoard,
    };
  },

  [dashBoardActionTypes.HANDLE_FINISH_DASH_BOARD_CARD]: (state, action) => {
    const { finishDateTime } = action.payload;
    const { columnId, id } = state.currentCard;

    const dashBoard = {
      ...state.currentDashboard,
    };

    const date = formatDate({ isFullDate: false, currentDateString: finishDateTime });

    const currentFormatDate = `${date.formatDate} ${date.hours}:${date.minutes}`;

    const newCardList = dashBoard.columns[columnId].cards.map((card) => (
      card.id === id ? { ...card, isDone: true, finishDateTime: currentFormatDate } : card
    ));

    const newColumnList = dashBoard.columns.map((column) => (
      column.id === columnId ? { ...column, cards: [...newCardList] } : column
    ));

    return {
      ...state,
      currentDashboard: {
        ...state.currentDashboard,
        columns: newColumnList,
      },
    };
  },

  [dashBoardActionTypes.HANDLE_SAVE_CARD_COMMENT]: (state, action) => {
    const { comment } = action.payload;
    const { columnId, id } = state.currentCard;

    const dashBoard = {
      ...state.currentDashboard,
    };

    const newCardList = dashBoard.columns[columnId].cards.map((card) => (
      card.id === id ? { ...card, comment } : card
    ));

    const newColumnList = dashBoard.columns.map((column) => (
      column.id === columnId ? { ...column, cards: [...newCardList] } : column
    ));

    return {
      ...state,
      currentDashboard: {
        ...state.currentDashboard,
        columns: newColumnList,
      },
    };
  },

  [dashBoardActionTypes.SET_CURRENT_CARD_DATA]: (state, action) => {
    const { columnId, cardId } = action.payload;

    const dashBoard = {
      ...state.currentDashboard,
    };

    const currentCard = dashBoard.columns[columnId].cards.find((card) => card.id === cardId);

    return {
      ...state,
      currentCard,
    };
  },

  [dashBoardActionTypes.DASH_BOARD_HANDLE_MOVE_COLUMN]: (state, action) => {
    const { source, destination } = action.payload;
    const updatedBoard = moveColumn(state.currentDashboard, source, destination);

    return {
      ...state,
      currentDashboard: updatedBoard,
    };
  },

  [dashBoardActionTypes.DASH_BOARD_HANDLE_ADD_NEW_COLUMN]: (state, action) => {
    const { column } = action.payload;
    const updatedBoard = addColumn(state.currentDashboard, column);

    return {
      ...state,
      currentDashboard: updatedBoard,
    };
  },

};

export default handleActions(handlerMap, initialState);
